var request = require('request'); // For external requests
var express = require('express'); // For routing
var app = express();

// Create a main route
app.get('/', function(req, res) {
    res.send('Home page');
});

// Begin of user routes: add, get one and all users
// Create route to get all users from Flask API with Get method
app.get('/users', function (req, res) {
  request('http://127.0.0.1:5000/users', function(error, response, body) {
    if (!error && response.statusCode == 200) {
        // response.send(body);
        console.log(body);
    } else {
      console.log(error);
      // response.send(error);
    }
  })
});

// Create route to get one user from Flask API with Get method
app.get('/user', function(req, res) {
  res.send('Get one user');
  request('http://127.0.0.1:5000/user/aziz', function(err, res, body){
    if (!err && res.statusCode == 200) {
        // res.send(body);
        return console.log(body);
    } else {
      // res.send(error);
      console.log(error);
    }
  })
});

// Create route to send a new user to Flask API with Get and POST method
app.get('/user/new', function(req, res) {
    res.send('Add new user');
    var options = {
      uri: 'http://127.0.0.1:5000/user',
      method: 'POST',
      json: {
        "first_name": "User",
        "last_name": "toto"
      }
    };

    request(options, function (error, response, body) {
      if (!error && response.statusCode == 200) {
        console.log(body)
      }
    });
});
// End of user routes: add, get one or all users


// Begin of notaire routes: add, get one and all notaires
// Create route to get all users from Flask API with Get method
app.get('/notaires', function (req, res) {
  request('http://127.0.0.1:5000/notaires', function(error, response, body) {
    if (!error && response.statusCode == 200) {
        // response.send(body);
        console.log(body);
    } else {
      console.log(error);
      // response.send(error);
    }
  })
});

// Create route to get one notaire from Flask API with Get method
app.get('/notaire', function(req, res) {
  res.send('Get one notaire');
  request('http://127.0.0.1:5000/notaire/aziz', function(err, res, body){
    if (!err && res.statusCode == 200) {
        // res.send(body);
        return console.log(body);
    } else {
      // res.send(error);
      console.log(error);
    }
  })
});

// Create route to send a new notaire to Flask API with Get and POST method
app.get('/notaire/new', function(req, res) {
    res.send('Add new notaire');
    var options = {
      uri: 'http://127.0.0.1:5000/notaire',
      method: 'POST',
      json: {
        "first_name": "Notaire",
        "last_name": "Toto"
      }
    };

    request(options, function (error, response, body) {
      if (!error && response.statusCode == 200) {
        console.log(body)
      }
    });
});
// End of notaire routes: add, get one or all notaires


// Begin of rendez-vous(rdv) routes: add, get one and all rendez-vous(rdv)
// Create route to get all rdvs from Flask API with Get method
app.get('/rdvs', function (req, res) {
  request('http://127.0.0.1:5000/rdvs', function(error, response, body) {
    if (!error && response.statusCode == 200) {
        // response.send(body);
        console.log(body);
    } else {
      console.log(error);
      // response.send(error);
    }
  })
});

// Create route to get one rdv from Flask API with Get method
app.get('/rdv', function(req, res) {
  res.send('Get one rdv');
  request('http://127.0.0.1:5000/rdv/consultation', function(err, res, body){
    if (!err && res.statusCode == 200) {
        // res.send(body);
        return console.log(body);
    } else {
      // res.send(error);
      console.log(error);
    }
  })
});

// Create route to send a new rdv to Flask API with Get and POST method
app.get('/rdv/new', function(req, res) {
    res.send('Add new rdv');
    var options = {
      uri: 'http://127.0.0.1:5000/rdv',
      method: 'POST',
      json: {
        "title": "Rendez-vous with Notaire",
        "notaire_id": 1,
        "user_id": 3,
        "rdv_time": "09:45 AM"
      }
    };

    request(options, function (error, response, body) {
      if (!error && response.statusCode == 200) {
        console.log(body)
      }
    });
});
// End of rdv routes: add, get one or all rdv


// Begin of admin routes: add, get one and all admins
// Create route to get all admins from Flask API with Get method
app.get('/admins', function (req, res) {
  request('http://127.0.0.1:5000/admins', function(error, response, body) {
    if (!error && response.statusCode == 200) {
        // response.send(body);
        console.log(body);
    } else {
      console.log(error);
      // response.send(error);
    }
  })
});

// Create route to get one admin from Flask API with Get method
app.get('/admin', function(req, res) {
  res.send('Get one admin');
  request('http://127.0.0.1:5000/admin/aziz', function(err, res, body){
    if (!err && res.statusCode == 200) {
        // res.send(body);
        return console.log(body);
    } else {
      // res.send(error);
      console.log(error);
    }
  })
});

// Create route to send a new admin to Flask API with Get and POST method
app.get('/admin/new', function(req, res) {
    res.send('Add new admin');
    var options = {
      uri: 'http://127.0.0.1:5000/admin',
      method: 'POST',
      json: {
        "username": "Admin",
        "password": "passwd"
      }
    };

    request(options, function (error, response, body) {
      if (!error && response.statusCode == 200) {
        console.log(body)
      }
    });
});
// End of admin routes: add, get one or all admins


app.listen(3000, function () {
  console.log('Example app listening on port 3000!')
})
